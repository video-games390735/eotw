﻿using System.Collections;
using System.Diagnostics.CodeAnalysis;
using Cinemachine;
using UnityEngine;

[SuppressMessage("ReSharper", "Unity.PerformanceCriticalCodeInvocation")]
public static class CameraUtils
{
    private const float DEFAULT_WAIT_TIME = 4.0f;
    private const float DEFAULT_RECENTER_TIME = 0.7f;

    private static IEnumerator cameraRecenterRoutine;
    public static readonly CinemachineFreeLook CameraController = GetCameraController();

    public static void EnableCameraRecenter(MonoBehaviour monoBehaviourRef, float waitTime = DEFAULT_WAIT_TIME,
        float recenterTime = DEFAULT_RECENTER_TIME, float disableAfterSeconds = 0)
    {
        if (!(cameraRecenterRoutine is null)) monoBehaviourRef.StopCoroutine(cameraRecenterRoutine);
        cameraRecenterRoutine = EnableRecenter(waitTime, recenterTime, disableAfterSeconds);
        monoBehaviourRef.StartCoroutine(cameraRecenterRoutine);
    }

    public static void DisableCameraRecenter()
    {
        CameraController.m_RecenterToTargetHeading.m_enabled = false;
        CameraController.m_YAxisRecentering.m_enabled = false;
    }

    private static CinemachineFreeLook GetCameraController()
    {
        return GameObject.FindWithTag("CameraController").GetComponent<CinemachineFreeLook>();
    }

    public static Camera GetPlayerCamera()
    {
        return GameObject.FindWithTag("PlayerCamera").GetComponent<Camera>();
    }

    public static CinemachineBrain GetPlayerCameraBrain()
    {
        return GameObject.FindWithTag("PlayerCamera").GetComponent<CinemachineBrain>();
    }


    private static IEnumerator EnableRecenter(float waitTime, float recenterTime, float disableAfterSeconds)
    {
        yield return new WaitForEndOfFrame();
        SetTargetHeadingTiming(waitTime, recenterTime, false);
        yield return new WaitForSecondsRealtime(waitTime);

        SetTargetHeadingTiming(waitTime, recenterTime, true);

        if (disableAfterSeconds == 0) yield break;
        yield return new WaitForSecondsRealtime(disableAfterSeconds);
        DisableCameraRecenter();
    }

    private static void SetTargetHeadingTiming(float waitTime, float recenterTime, bool enabled)
    {
        CameraController.m_RecenterToTargetHeading.m_WaitTime = waitTime;
        CameraController.m_YAxisRecentering.m_WaitTime = waitTime;
        CameraController.m_RecenterToTargetHeading.m_RecenteringTime = recenterTime;
        CameraController.m_YAxisRecentering.m_RecenteringTime = recenterTime;

        CameraController.m_RecenterToTargetHeading.m_enabled = enabled;
        CameraController.m_YAxisRecentering.m_enabled = enabled;
    }
}