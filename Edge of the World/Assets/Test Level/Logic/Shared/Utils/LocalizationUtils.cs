﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Localization.Settings;
using UnityEngine.ResourceManagement.AsyncOperations;

public static class LocalizationUtils
{
    public static MonoBehaviour monoBehaviour;

    public static IEnumerator SetLanguage(int languageIndex)
    {
        yield return LocalizationSettings.InitializationOperation;
        LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[languageIndex];
    }

    private static IEnumerator GetLocalizedString(string table, string key, Action<string> callback)
    {
        yield return LocalizationSettings.InitializationOperation;
        AsyncOperationHandle operation = LocalizationSettings.StringDatabase.GetLocalizedStringAsync(table, key);
        yield return operation;
        callback(operation.Result as string);
    }

    public static void Translate(string table, string key, Action<string> callback)
    {
        monoBehaviour.StartCoroutine(GetLocalizedString(table, key, callback));
    }
}