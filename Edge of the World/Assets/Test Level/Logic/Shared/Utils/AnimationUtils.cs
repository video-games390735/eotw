﻿using UnityEngine;

public static class AnimationUtils
{
    public static AnimatorClipInfo? GetCurrentClip(Animator objectAnimator)
    {
        AnimatorClipInfo[] animatorInfo = objectAnimator.GetCurrentAnimatorClipInfo(0);

        if (animatorInfo.Length < 1) return null;
        AnimatorClipInfo currentClip = animatorInfo[0];

        foreach (AnimatorClipInfo clipInfo in animatorInfo)
            if (clipInfo.weight > currentClip.weight)
                currentClip = clipInfo;

        return currentClip;
    }

    public static bool IsCharacterMoving(Animator objectAnimator)
    {
        // check if the character walks, runs or crouches
        return objectAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Directional");
    }
}