﻿using UnityEngine;

public interface IPlayer
{
    public bool IsInteracting { get; set; }
    public Camera Camera { get; }
    public Vector3 ShoulderPosition { get; set; }
    public Rigidbody Rigidbody { get; }
}
