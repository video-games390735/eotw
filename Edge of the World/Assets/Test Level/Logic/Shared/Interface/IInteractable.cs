﻿using UnityEngine;

public interface IInteractable
{
    public string Name { get; set; }
    public bool IsInteracting { get; set; }
    public float DistanceToPlayer { get; set; }
    public bool CanInteractWithPlayer { get; set; }
    public bool CanShowOutline { get; set; }
    public Collider TriggerAreaCollider { get; set; }
    public float TriggerZoneRadius { get; }
    public InteractableActionTypes CurrentActionType { set; get; }
    public string CurrentActionName { get; }
    public Vector3 ObjectCenter { get; }
}