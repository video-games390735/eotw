﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CharacterEnvironmentInteractionService
{
    public static readonly List<IInteractable> EmitterStack = new List<IInteractable>();
    public static event Action<InteractionEventTypes, ICharacter, IInteractable> Event;

    public static void NotifyEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactable = null)
    {
        Event?.Invoke(eventType, character, interactable);
    }

    public static IInteractable GetClosestObjectToPlayer()
    {
        IInteractable closestInteractable = EmitterStack.Find(interactable =>
            interactable.CanInteractWithPlayer);
        
        if (closestInteractable is null) return null;
        
        foreach (IInteractable interactable in EmitterStack.Where(interactable =>
            interactable.CanInteractWithPlayer))
        
            if (interactable.DistanceToPlayer < closestInteractable.DistanceToPlayer)
                closestInteractable = interactable;
        return closestInteractable;
    }

    public static Vector3 GetCharacterOffsetFromInteractable(IInteractable interactable, ICharacter characterEntity)
    {
        // We can use this to determine on which side of an object the character is and also the relative distance between them
        return ((IEntity)interactable).Transform.InverseTransformPoint(characterEntity.Transform.position);
    }
}