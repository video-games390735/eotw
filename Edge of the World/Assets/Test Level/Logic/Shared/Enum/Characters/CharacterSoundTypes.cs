﻿using System;

[Serializable]
public enum CharacterSoundTypes
{
    WalkingFootsteps,
    RunningFootsteps,
    CrouchingFootsteps,
    Interjections,
    Landing,
    Falling,
    HardLanding,
    Jumping
}