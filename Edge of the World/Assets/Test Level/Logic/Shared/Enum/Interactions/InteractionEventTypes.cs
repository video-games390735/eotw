﻿using System;

[Serializable]
public enum InteractionEventTypes
{
    //General use events
    CharacterEnter,
    CharacterExit,
    CharacterInteraction,
    AnimationExit,
    CharacterTeleport,
    
    ItemInteraction,
    BedInteraction,
    BenchInteraction,
    DoorInteraction,
    LadderInteraction,
    MovableObjectInteraction,
    DwellerInteraction
    
}