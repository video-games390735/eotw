﻿using UnityEngine;

public class InteractableDoor : InteractableObject
{
    public InteractableDoor(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation) : base(
        gameObjectRef, interactableCategoryTemplate, objectTemplate, handPosition, handRotation)
    {
    }
}