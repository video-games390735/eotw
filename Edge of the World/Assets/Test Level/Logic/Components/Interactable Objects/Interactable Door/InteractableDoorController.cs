﻿using UnityEngine;

public class InteractableDoorController: InteractableObjectController
{
    protected override IInteractableObject GetInteractableObjectInstance()
    {
        return new InteractableDoor(gameObject, categoryTemplate, objectTemplate, handPosition, handRotation);
    }

    protected override void Update()
    {    //TODO add condition when to update
        base.Update();
        SetVisualMarkerPosition(CharacterEnvironmentInteractionService.GetCharacterOffsetFromInteractable(interactableObjectEntity, playerEntity).z > 0);
    }

    protected override void UpdateOutline()
    {        SetLayer(interactableObjectEntity.CanInteractWithPlayer && !((IPlayer) playerEntity).IsInteracting ? 13 : 0, true);
    }
}