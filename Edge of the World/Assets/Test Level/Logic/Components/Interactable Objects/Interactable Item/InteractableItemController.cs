﻿public class InteractableItemController : InteractableObjectController
{
    protected override IInteractableObject GetInteractableObjectInstance()
    {
        return new InteractableItem(gameObject, categoryTemplate, objectTemplate, handPosition, handRotation);
    }
}