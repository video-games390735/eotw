﻿using UnityEngine;

public class InteractableItem : InteractableObject
{
    public InteractableItem(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation) : base(
        gameObjectRef, interactableCategoryTemplate, objectTemplate, handPosition, handRotation) //TODO move hand rotation/position in objectTemplate
    {
    }
}