using UnityEngine;

public class InteractableMovableObject : InteractableObject
{
    public InteractableMovableObject(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation) : base(
        gameObjectRef, interactableCategoryTemplate, objectTemplate, handPosition, handRotation)
    {
    }
}