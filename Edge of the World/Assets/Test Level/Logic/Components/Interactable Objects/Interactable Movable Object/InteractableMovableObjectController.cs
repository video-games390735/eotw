public class InteractableMovableObjectController : InteractableObjectController
{
    protected override IInteractableObject GetInteractableObjectInstance()
    {
        return new InteractableMovableObject(gameObject, categoryTemplate, objectTemplate, handPosition, handRotation);
    }
}