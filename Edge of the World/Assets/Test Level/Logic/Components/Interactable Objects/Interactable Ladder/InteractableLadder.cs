﻿using UnityEngine;

public class InteractableLadder : InteractableObject
{
    public InteractableLadder(GameObject gameObjectRef, InteractableCategoryTemplate interactableCategoryTemplate,
        InteractableObjectTemplate objectTemplate, Vector3 handPosition, Vector3 handRotation) : base(
        gameObjectRef, interactableCategoryTemplate, objectTemplate, handPosition, handRotation)
    {
    }
}