﻿using System;
using System.Globalization;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class DebugMenuController : MonoBehaviour
{
    public Button systemInfoButton;
    public Text systemInfoText;
    public Button exitGameButton;
    public Button toolsButton;
    public Text toolsText;
    public Button takeScreenshotButton;
    
    private PlayerInputControls inputControls;
    private bool isMenuVisible;
    private bool isSystemInfoVisible;
    private bool isToolsVisible;

    private void Start()
    {
        if (Debug.isDebugBuild)
        {
            SetInputActions();
            SetButtonListeners();
        }

        gameObject.SetActive(false);
    }

    private void SetInputActions()
    {
        inputControls = InputService.inputControls;
        inputControls.Debug.ShowSystemInfo.performed += _ => SetSystemInfoVisible();
        inputControls.Debug.TakeScreenshot.performed += _ => TakeScreenshot();
        inputControls.Debug.ShowTools.performed += _ => SetToolsVisible();
        inputControls.Debug.ExitGame.performed += _ => ExitGame();
    }

    private void SetButtonListeners()
    {
        systemInfoButton.onClick.AddListener(SetSystemInfoVisible);
        toolsButton.onClick.AddListener(SetToolsVisible);
        takeScreenshotButton.onClick.AddListener(TakeScreenshot);
        exitGameButton.onClick.AddListener(ExitGame);
    }

    private void SetSystemInfoVisible()
    {
        isSystemInfoVisible = !isSystemInfoVisible;
        systemInfoText.text = isSystemInfoVisible ? "    Hide System Info" : "    Show System Info";
        DebugService.NotifyEvent(DebugEventTypes.ShowSystemInfo, isSystemInfoVisible);
    }
    
    private void SetToolsVisible()
    {
        isToolsVisible = !isToolsVisible;
        toolsText.text = isToolsVisible ? "    Hide Tools" : "    Show Tools";
        DebugService.NotifyEvent(DebugEventTypes.ShowTools, isToolsVisible);
    }

    private static void TakeScreenshot()
    {
        DateTime currentDateTime = DateTime.Now;
        CultureInfo culture = new CultureInfo("de-DE");
        string formattedDateTime = currentDateTime.ToString(culture);

        
        string screenshotPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).Replace("\\", "/") + "/Edge of the World/";
        screenshotPath += "Screenshot - " + formattedDateTime.Replace(":", ".") + ".png";

        Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments).Replace("\\", "/") + "/Edge of the World/");

        ScreenCapture.CaptureScreenshot(screenshotPath, 1);
    }

    private static void ExitGame()
    {
        DebugService.NotifyEvent(DebugEventTypes.ExitGame);
    }
}