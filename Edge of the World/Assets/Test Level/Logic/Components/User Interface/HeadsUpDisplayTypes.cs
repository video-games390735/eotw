﻿public enum HeadsUpDisplayTypes
{
    Health,
    Compass,
    ActiveQuests,
    ContextualPrompt
}