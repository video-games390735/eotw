﻿using UnityEngine;

//TODO add documentation for new tool (generate folders)
public class GameHudController : MonoBehaviour //TODO refactor the whole folder
{
    public GameObject contextualPromptView;
    public UserInterfaceTextTemplate contextualPromptViewTextTemplate; //TODO make array with two properties for every element(view type, template)

    public Sprite actionKey; //TODO to be removed
    private GameHud gameHudEntity;

    private void Awake()
    {
        gameHudEntity = new GameHud(gameObject.GetComponent<Canvas>(), contextualPromptView, contextualPromptViewTextTemplate);
        gameHudEntity.ContextualPromptView.ActionKey.sprite = actionKey;
    }

    private void FixedUpdate()
    {
        SetContextualPromptView();
    }

    private void SetContextualPromptView()
    {
        IInteractable closestObjectToPlayer = CharacterEnvironmentInteractionService.GetClosestObjectToPlayer();
        gameHudEntity.ContextualPromptView.Container.SetActive(!(closestObjectToPlayer is null)); //remove usage to run in update
        if (closestObjectToPlayer is null) return;
        gameHudEntity.ContextualPromptView.ObjectName.text = closestObjectToPlayer.Name;
        gameHudEntity.ContextualPromptView.ActionType.text = closestObjectToPlayer.CurrentActionName;
    }

    private void SetVisibleViews()
    {
        //TODO to be implemented
    }
}