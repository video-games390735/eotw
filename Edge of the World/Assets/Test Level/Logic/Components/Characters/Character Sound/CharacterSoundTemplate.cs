﻿using UnityEngine;

[CreateAssetMenu(fileName = "Character Sound Group", menuName = "Character/Character Sound Group", order = 0)]
public class CharacterSoundTemplate : ScriptableObject
{
    public CharacterSoundGroup[] soundGroup;
}