﻿using System;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class CharacterSoundController
{
    private readonly ICharacter characterEntity;

    public CharacterSoundController(ICharacter characterEntity)
    {
        this.characterEntity = characterEntity;
    }

    public void OnSoundEvent(CharacterSoundTypes eventType)
    {
        switch (eventType)
        {
            case CharacterSoundTypes.Interjections:
            case CharacterSoundTypes.Landing:
            case CharacterSoundTypes.Falling:
            case CharacterSoundTypes.HardLanding:
            case CharacterSoundTypes.Jumping:
                PlaySound(eventType);
                break;
            default:
                return;
        }
    }

    public void OnFootstepSoundEvent(AnimationEvent animationEvent)
    {
        // footstep sounds are a special case where we have to make sure they don't overlap
        CharacterSoundTypes eventType = (CharacterSoundTypes) Enum.Parse(typeof(CharacterSoundTypes), animationEvent.stringParameter);

        switch (eventType)
        {
            case CharacterSoundTypes.WalkingFootsteps:
            case CharacterSoundTypes.RunningFootsteps:
            case CharacterSoundTypes.CrouchingFootsteps:
                if (animationEvent.animatorClipInfo.clip.name != AnimationUtils.GetCurrentClip(characterEntity.Animator)?.clip.name ||
                    !((IMobile)characterEntity).IsGrounded) return;
                PlaySound(eventType);
                break;
            default:
                return;
        }
    }

    private void PlaySound(CharacterSoundTypes characterSoundType)
    {
        ICharacterSoundGroup selectedCharacterSoundCategory = characterEntity.SoundGroup.FirstOrDefault(sound => sound.Name == characterSoundType);

        if (selectedCharacterSoundCategory == null) return;

        characterEntity.AudioSource.PlayOneShot(
            selectedCharacterSoundCategory.Sounds[Random.Range(0, selectedCharacterSoundCategory.Sounds.Length)],
            selectedCharacterSoundCategory.Volume);
    }
}