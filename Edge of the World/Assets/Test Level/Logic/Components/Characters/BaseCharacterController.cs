﻿using JetBrains.Annotations;
using UnityEngine;

public abstract class
    BaseCharacterController : MonoBehaviour, IController
{
    //TODO Check fields used in inspector but should not be public. make them serialized 

    public CharacterSoundTemplate characterSoundTemplate;
    protected ICharacter characterEntity;

    private CharacterSoundController characterSoundController;

    protected virtual void Awake()
    {
        characterEntity = GetCharacterInstance();
        characterSoundController = new CharacterSoundController(characterEntity);
    }

    public T GetEntity<T>()
    {
        return (T) characterEntity;
    }

    [UsedImplicitly]
    public void NotifyFootstepSoundEvent(AnimationEvent animationEvent)
    {
        characterSoundController.OnFootstepSoundEvent(animationEvent);
    }

    [UsedImplicitly]
    public void NotifySoundEvent(CharacterSoundTypes characterSoundType)
    {
        characterSoundController.OnSoundEvent(characterSoundType);
    }

    protected abstract ICharacter GetCharacterInstance();
}