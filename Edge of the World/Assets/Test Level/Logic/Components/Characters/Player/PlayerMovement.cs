﻿using System;
using UnityEngine;

public class PlayerMovement : CharacterMovement
{
    private readonly PlayerInputControls inputControls;

    private float turnVelocity;

    public PlayerMovement(ICharacter characterEntity, CharacterMovementTemplate movementTemplate) : base(characterEntity, movementTemplate)
    {
        inputControls = InputService.inputControls;
        SetInputActions();
    }

    public override void FixedUpdate()
    {
        base.FixedUpdate();
        AddMomentumOnFall();
    }
    
    private void SetInputActions()
    {
        inputControls.Movement.Sprint.performed += _ => characterMovementEntity.CanRun = true;
        inputControls.Movement.Sprint.canceled += _ => characterMovementEntity.CanRun = false;
        inputControls.Movement.Jump.performed += _ => AddJumpForce();
        inputControls.Movement.Crouch.performed += _ => Crouch();
    }

    protected override void SetCharacterRotation()
    {
        Vector3 directionVector = GetDirectionVector().normalized;

        if (!(directionVector.magnitude >= 0.05f) ||
            !characterMovementEntity.CanRotate ||
            characterEntity.Animator.GetCurrentAnimatorStateInfo(0).IsName("Hard Land") ||
            characterEntity.Animator.GetCurrentAnimatorStateInfo(0).IsName("Jump From Idle")) return;

        // Smooth rotation taking into consideration camera direction
        float targetAngle = Mathf.Atan2(directionVector.x, directionVector.z) * Mathf.Rad2Deg + ((Player) characterEntity).Camera.transform.eulerAngles.y;
        float currentAngle = Mathf.SmoothDampAngle(characterEntity.Transform.eulerAngles.y, targetAngle, ref turnVelocity, movementTemplate.turnSpeed);
        characterEntity.Transform.rotation = Quaternion.Euler(0f, currentAngle, 0f);
    }

    protected override void MoveCharacter()
    {
        float speedLimit = characterMovementEntity.CanRun && !characterMovementEntity.IsCrouching ? 1f : 0.5f;
        if (!characterMovementEntity.CanMove) speedLimit = 0f;

        float slopeValue = GetSlopeValue();
        float inputVelocity = Mathf.Clamp(GetDirectionVector().magnitude, 0.01f, speedLimit);

        if (Math.Abs(speedLimit - 0.5f) < 0.01f)
            //prevent aggressive slowing when walking on steep surfaces
            slopeValue = Mathf.Clamp(slopeValue / 4f + 0.75f, 0.75f, 1);

        characterEntity.Animator.SetFloat(Movement, inputVelocity * slopeValue, 0.15f, Time.deltaTime);
        SetSurfaceFriction();
        characterMovementEntity.IsRunning = characterEntity.Animator.GetFloat(Movement) > 0.55f;
    }

    private void AddJumpForce()
    {
        //TODO jump from walk still has camera jump
        if (!characterMovementEntity.IsGrounded || !AnimationUtils.IsCharacterMoving(characterEntity.Animator) || !(GetSlopeValue() > 0.4f) ||
            characterMovementEntity.IsCrouching || !characterMovementEntity.CanJump) return;

        characterEntity.Animator.SetBool(Jump, true);

        if (characterEntity.Animator.GetFloat(Movement) > 0.3f)
            // cancel gravity pulling down player when jumping
            ((Player) characterEntity).Rigidbody.AddForce(Vector3.up * movementTemplate.jumpForce, ForceMode.Impulse);
    }

    private void Crouch()
    {
        if (!CanCrouch()) return;
        if (AnimationUtils.IsCharacterMoving(characterEntity.Animator) && characterMovementEntity.IsGrounded)
            characterMovementEntity.IsCrouching = !characterMovementEntity.IsCrouching;
    }

    private Vector3 GetDirectionVector()
    {
        float verticalInput = inputControls.Movement.Directional.ReadValue<Vector2>().y;
        float horizontalInput = inputControls.Movement.Directional.ReadValue<Vector2>().x;

        return new Vector3(horizontalInput, 0f, verticalInput);
    }

    private void SetSurfaceFriction()
    {
        // prevent getting stuck on vertical surfaces and sliding on steep surfaces
        if (GetDirectionVector().magnitude > 0 || !characterMovementEntity.IsGrounded)
            characterEntity.Collider.material.frictionCombine = PhysicMaterialCombine.Minimum;
        else
            characterEntity.Collider.material.frictionCombine = PhysicMaterialCombine.Maximum;
    }

    private bool CanCrouch()
    {
        //TODO add methods for can jump etc..
        Bounds bounds = characterEntity.Collider.bounds;
        return !Physics.Raycast(new Vector3(bounds.center.x, bounds.center.y + bounds.extents.y - 0.1f, bounds.center.z), characterEntity.Transform.up,
            out RaycastHit hit, 0.5f, Physics.DefaultRaycastLayers) || !characterMovementEntity.IsCrouching;
    }
    
    private void AddMomentumOnFall()
    {
       bool isFalling = false;
        foreach (AnimatorClipInfo animationState in characterEntity.Animator.GetCurrentAnimatorClipInfo(0))
            if (animationState.clip.name == "Falling")
                isFalling = true;
    
        if (!isFalling || characterMovementEntity.IsGrounded) return;
    
        // preserve falling momentum
        Vector3 characterForward = characterEntity.Transform.forward;
        ((Player)characterEntity).Rigidbody.AddForce(new Vector3(characterForward.x, 0, characterForward.z) * movementTemplate.fallingForwardForce);
    }
}