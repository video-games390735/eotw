﻿using System;
using Cinemachine;
using JetBrains.Annotations;
using UnityEngine;

public class PlayerController : BaseCharacterController
{
    private static readonly int IsInteracting = Animator.StringToHash("isInteracting");

    //TODO set Can & Is properties as it should
    //TODO sort methods and properties in every class
    //TODO add missing sounds
    //TODO add missing interfaces
    public float cameraColliderPadding = 2.0f;
    public Transform cameraTarget;
    public CharacterMovementTemplate characterMovementTemplate;
    public Vector3 leftCameraTarget;
    public Vector3 centerCameraTarget;
    public Vector3 rightCameraTarget;

    private PlayerInputControls inputControls;
    private PlayerMovement playerMovement;
    private bool isCameraRecenterEnabled;


    protected override void Awake()
    {
        base.Awake();
        InputService.Initialize(); //TODO move this to a separate controller 
        SetInputActions();

        LocalizationUtils.monoBehaviour = this; // TODO move this to a separate controller
        Cursor.visible = false; // TODO move this to a separate controller
        
        playerMovement = new PlayerMovement(characterEntity, characterMovementTemplate);

        ((IPlayer) characterEntity).ShoulderPosition = rightCameraTarget;
    }

    private void Update()
    {
        SetAnimatorParams();
        playerMovement.Update();

        if (((IPlayer) characterEntity).IsInteracting)
        {
            isCameraRecenterEnabled = false;
            return;
        }

        if (AnimationUtils.IsCharacterMoving(characterEntity.Animator) && ((IPlayer) characterEntity).Rigidbody.velocity.magnitude < 0.001f &&
            !isCameraRecenterEnabled)
        {
            CameraUtils.EnableCameraRecenter(this);
            isCameraRecenterEnabled = true;
        }

        if (!(((IPlayer) characterEntity).Rigidbody.velocity.magnitude >= 0.001f)) return;
        CameraUtils.DisableCameraRecenter();
        isCameraRecenterEnabled = false;

    }

    private void FixedUpdate()
    {
        SetCameraTarget();

        playerMovement.FixedUpdate();
    }

    private void OnEnable()
    {
        inputControls.Enable();
    }

    private void OnDisable()
    {
        inputControls.Disable();
    }

    private void OnAnimatorIK(int layerIndex)
    {
        AnimationEventsService.NotifyEvent(AnimationEventTypes.AnimatorIK, characterEntity);
    }
    
    private void SetAnimatorParams()
    {
        characterEntity.Animator.SetBool(IsInteracting, ((IPlayer)characterEntity).IsInteracting);
        AnimationEventsService.NotifyEvent(AnimationEventTypes.ParamChanges, characterEntity);
    }

    [UsedImplicitly]
    public void NotifyInteractionEvent(InteractionEventTypes eventType)
    {
        CharacterEnvironmentInteractionService.NotifyEvent(eventType, characterEntity);
    }

    [UsedImplicitly]
    protected void DisableJumpState()
    {
        playerMovement.DisableJumpState(); //TODO use triggers
    }

    [UsedImplicitly]
    private void DisableCrouchState()
    {
        // a special case for jumping in place where you can enable crouching immediately after jumping so we disable crouching in this case 
        playerMovement.DisableCrouchState();
    }

    protected override ICharacter GetCharacterInstance()
    {
        return new Player(gameObject, characterSoundTemplate.soundGroup);
    }

    private void SetInputActions()
    {
        inputControls = InputService.inputControls;
        inputControls.Camera.ChangeShoulderSide.performed += _ => ChangeShoulderSide();
        inputControls.Interaction.Interact.performed +=
            _ => CharacterEnvironmentInteractionService.NotifyEvent(InteractionEventTypes.CharacterInteraction, characterEntity);
    }

    private void ChangeShoulderSide()
    {
        ((IPlayer) characterEntity).ShoulderPosition =
            ((IPlayer) characterEntity).ShoulderPosition == leftCameraTarget ? rightCameraTarget : leftCameraTarget;
    }

    private void SetCameraTarget()
    {
        Vector3 positionReference = Vector3.zero;
        Vector3 targetPosition = ((IPlayer) characterEntity).ShoulderPosition;

        Vector3 raycastStartPosition = characterEntity.Transform.TransformPoint(centerCameraTarget);
        Vector3 raycastEndPosition = Vector3.zero + targetPosition;

        raycastEndPosition.x += Math.Sign(raycastEndPosition.x) * cameraColliderPadding;
        raycastEndPosition = characterEntity.Transform.TransformPoint(raycastEndPosition);

        if (Physics.Linecast(raycastStartPosition, raycastEndPosition, out RaycastHit hit, 1 << LayerMask.NameToLayer("Structure")) &&
            !hit.transform.CompareTag("Player"))
        {
            targetPosition = characterEntity.Transform.InverseTransformPoint(hit.point);
            targetPosition.x -= Math.Sign(targetPosition.x) * cameraColliderPadding * 2f;
        }

        cameraTarget.localPosition = Vector3.SmoothDamp(cameraTarget.localPosition, targetPosition, ref positionReference, 0.1f);
    }
}