﻿using UnityEngine;

public class DwellerController : BaseCharacterController
{
    public DwellerTemplate dwellerTemplate;
    private IInteractable dwellerEntity;
    private ICharacter playerEntity;

    protected override void Awake()
    {
        LocalizationUtils.monoBehaviour = this; // TODO move this to a separate controller
        base.Awake();

        dwellerEntity = characterEntity as IInteractable;
    }

    private void Start()
    {
        CharacterEnvironmentInteractionService.Event += OnInteractionEvent;
        playerEntity = GameUtils.GetPlayerEntity();
        GenerateTriggerArea();
    }

    protected void Update()
    {
        SetDistanceToPlayer();
        SetCanInteract();
        UpdateOutline();
    }

    protected override ICharacter GetCharacterInstance()
    {
        return new Dweller(gameObject, characterSoundTemplate.soundGroup, dwellerTemplate);
    }

    private void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactable)
    {
        if (!(interactable is ICharacter dweller) || dweller.GameObjectRef.GetInstanceID() != gameObject.GetInstanceID()) return;
        switch (eventType)
        {
            case InteractionEventTypes.CharacterEnter:
                dwellerEntity.CanShowOutline = true;
                CharacterEnvironmentInteractionService.EmitterStack.Add((IInteractable) characterEntity);
                break;
            case InteractionEventTypes.CharacterExit:
                dwellerEntity.CanShowOutline = false;
                CharacterEnvironmentInteractionService.EmitterStack.Remove((IInteractable) characterEntity);
                break;
            default:
                return;
        }
    }

    private void SetDistanceToPlayer()
    {
        dwellerEntity.DistanceToPlayer =
            Vector3.Distance(characterEntity.Transform.position, playerEntity.Transform.position);
    }

    private void GenerateTriggerArea()
    {
        GameObject triggerZone = new GameObject {name = "Trigger Zone"};

        triggerZone.transform.parent = gameObject.transform;
        triggerZone.transform.localPosition = Vector3.zero;
        triggerZone.layer = 2; // Ignore Raycasts
        if (dwellerEntity == null) return;

        dwellerEntity.TriggerAreaCollider = (SphereCollider) triggerZone.AddComponent(typeof(SphereCollider));
        DwellerTriggerZoneController objectTriggerZoneController =
            (DwellerTriggerZoneController) triggerZone.AddComponent(typeof(DwellerTriggerZoneController));
        if (dwellerEntity.TriggerAreaCollider is null) return;

        ((SphereCollider) dwellerEntity.TriggerAreaCollider).radius = dwellerEntity.TriggerZoneRadius;
        dwellerEntity.TriggerAreaCollider.isTrigger = true;
    }

    private void SetCanInteract()
    {
        dwellerEntity.CanInteractWithPlayer = dwellerEntity.DistanceToPlayer < dwellerTemplate.maxDistanceToPlayer;
    }

    private void UpdateOutline()
    {
        if (dwellerEntity.CanShowOutline && !((IPlayer) playerEntity).IsInteracting)
            SetLayer(dwellerEntity.CanInteractWithPlayer ? 11 : 12);
        else
            SetLayer(0);
    }

    private void SetLayer(int layerId)
    {
        characterEntity.GameObjectRef.layer = layerId;
        int childCount = characterEntity.GameObjectRef.transform.childCount;


        if (childCount == 0) return;
        for (int i = 0; i < childCount; i++)
        {
            GameObject child = characterEntity.GameObjectRef.transform.GetChild(i).gameObject;
            if (child.layer == 2)
                continue;
            child.layer = layerId;
        }
    }
}