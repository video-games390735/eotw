﻿using UnityEngine;

public class DwellerTriggerZoneController : MonoBehaviour
{
    private void OnTriggerEnter(Collider collidingObject)
    {
        if (!collidingObject.CompareTag("Player")) return;
        CharacterEnvironmentInteractionService.NotifyEvent(InteractionEventTypes.CharacterEnter, GetCharacterEntity(collidingObject.transform),
            GetInteractableEntity());
    }

    private void OnTriggerExit(Collider collidingObject)
    {
        if (!collidingObject.CompareTag("Player")) return;
        CharacterEnvironmentInteractionService.NotifyEvent(InteractionEventTypes.CharacterExit, GetCharacterEntity(collidingObject.transform),
            GetInteractableEntity());
    }

    private IInteractable GetInteractableEntity()
    {
        return transform.parent.GetComponent<IController>().GetEntity<IInteractable>();
    }

    private static ICharacter GetCharacterEntity(Component characterTransform)
    {
        return characterTransform.GetComponent<IController>().GetEntity<ICharacter>();
    }
}