﻿using UnityEngine;

public abstract class Character : ICharacter
{
    protected Character(GameObject gameObjectRef, ICharacterSoundGroup[] soundGroup)
    {
        GameObjectRef = gameObjectRef;
        SoundGroup = soundGroup;

        Animator = GameObjectRef.GetComponent<Animator>();
        Collider = GameObjectRef.GetComponent<Collider>();
        Transform = GameObjectRef.GetComponent<Transform>();
        Renderer = GameObjectRef.GetComponent<Renderer>();
        AudioSource = GameObjectRef.GetComponent<AudioSource>();
    }

    public Animator Animator { get; }
    public Collider Collider { get; }
    public Transform Transform { get; }
    public Renderer Renderer { get; }
    public AudioSource AudioSource { get; }
    public GameObject GameObjectRef { get; }
    public ICharacterSoundGroup[] SoundGroup { get; }
}