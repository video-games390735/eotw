﻿using System.Collections;
using UnityEngine;

public class CharacterLadderInteraction : CharacterObjectInteraction
{
    private static readonly int ClimbFromAbove = Animator.StringToHash("climbFromAbove");
    private static readonly int ClimbFromBelow = Animator.StringToHash("climbFromBelow");
    
    private Vector3 climbingPoint;
    
    public CharacterLadderInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity, IInteractable targetInteractableObject) :
        base(
            monoBehaviourRef, characterEntity, targetInteractableObject)
    {
    }
    
    public override void StartInteraction()
    {
        if (targetInteractableObject.ObjectType != InteractableObjectTypes.Ladder)
            return;
        
        SetCharacterKinematic();
        
        if (characterEntity.Transform.position.y < climbingPoint.y)
        {
            characterEntity.Animator.SetBool(ClimbFromBelow, true);
            monoBehaviourRef.StartCoroutine(ClimbTopLadderInteraction());
        }
        else
        {
            characterEntity.Animator.SetBool(ClimbFromAbove, true);
            monoBehaviourRef.StartCoroutine(DescendBottomLadderInteraction());
        }
    }
    
    protected override void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        switch (eventType)
        {
            case InteractionEventTypes.AnimationExit:
                FinishInteraction();
                break;
            default:
                return;
        }
    }
    
    private void SetClimbingPoint()
    {
        Bounds topLadderBounds = targetInteractableObject.Renderer.bounds;
        Vector3 topLadderPosition = targetInteractableObject.ObjectCenter;
        topLadderPosition = new Vector3(topLadderPosition.x, topLadderBounds.size.y / 2 + topLadderPosition.y - 1.7f,
            topLadderPosition.z);
        climbingPoint = topLadderPosition;
    }
    
    protected override IEnumerator GetInitialPositioningMethod()
    {
        return SetCharacterPosition();
    }
    
    private IEnumerator ClimbTopLadderInteraction()
    {
        while (characterEntity.Transform.position.y < climbingPoint.y) yield return new WaitForFixedUpdate();
    
        characterEntity.Animator.SetBool(ClimbFromBelow, false);
    }
    
    private IEnumerator DescendBottomLadderInteraction()
    {
        while (characterEntity.Transform.position.y > targetInteractableObject.Transform.position.y + 0.05f) yield return new WaitForFixedUpdate();
    
        characterEntity.Animator.SetBool(ClimbFromAbove, false);
        FinishInteraction();
    }
    
    protected override Vector2 GetCharacterPositionOffset()
    {
        SetClimbingPoint();
        return characterEntity.Transform.position.y < climbingPoint.y ? base.GetCharacterPositionOffset() : new Vector2(0.07f, 0);
    }
}