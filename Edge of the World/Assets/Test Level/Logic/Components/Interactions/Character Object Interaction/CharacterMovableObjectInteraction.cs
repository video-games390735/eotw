using System.Collections;
using UnityEngine;

public class CharacterMovableObjectInteraction : CharacterObjectInteraction
{
    private static readonly int Push = Animator.StringToHash("pushObject");
    private static readonly int Pull = Animator.StringToHash("pullObject");

    public CharacterMovableObjectInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity,
        IInteractable targetInteractableObject) : base(
        monoBehaviourRef, characterEntity, targetInteractableObject)
    {
    }

    public override void StartInteraction()
    {
        if (targetInteractableObject.ObjectType != InteractableObjectTypes.MovableObject)
            return;
        switch (targetInteractableObject.CurrentActionType)
        {
            case InteractableActionTypes.Push:
                characterEntity.Animator.SetBool(Push, true);
                break;
            case InteractableActionTypes.Pull:
                characterEntity.Animator.SetBool(Pull, true);
                monoBehaviourRef.StartCoroutine(StopMovableObject());
                break;
        }

        base.StartInteraction();
    }

    protected override void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject)
    {
        switch (eventType)
        {
            case InteractionEventTypes.MovableObjectInteraction:
                if (targetInteractableObject.CurrentActionType == InteractableActionTypes.Push)
                {
                    characterEntity.Animator.SetBool(Push, false);
                    targetInteractableObject.Animator.SetBool(Push, true);
                    targetInteractableObject.CurrentActionType = InteractableActionTypes.Pull;
                }
                else
                {
                    targetInteractableObject.Animator.SetBool(Push, false);
                    targetInteractableObject.CurrentActionType = InteractableActionTypes.Push;
                }
                break;
            case InteractionEventTypes.AnimationExit:
                FinishInteraction();
                break;
            default:
                return;
        }
    }

    protected override IEnumerator GetInitialPositioningMethod()
    {
        return SetCharacterPosition();
    }

    protected override Vector3 GetTargetObjectPosition()
    {
        return targetInteractableObject.Transform.position;
    }

    private IEnumerator StopMovableObject()
    {
        while (targetInteractableObject.Transform.localPosition.x < -0.19f) yield return new WaitForFixedUpdate();
        characterEntity.Animator.SetBool(Pull, false);
    }
}