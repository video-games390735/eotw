﻿using System.Collections;
using UnityEngine;

public abstract class CharacterObjectInteraction : ICharacterEnvironmentInteraction
{
    private const float POSITIONING_DURATION = 0.5f;
    private const float ROTATING_DURATION = 0.5f;

    private static readonly int HandWeight = Animator.StringToHash("handWeight");
    private static readonly int Mirror = Animator.StringToHash("mirror");

    protected readonly ICharacter characterEntity;
    protected readonly IMobile characterMovement;
    protected readonly MonoBehaviour monoBehaviourRef;
    private readonly IPlayer playerEntity;

    private float interpolatingTimer;
    protected float targetIKWeight;
    protected IInteractableObject targetInteractableObject;
    private Vector3 targetInteractableObjectPosition;

    private bool useObjectSide;

    //TODO add highlight rotation to interactable object

    protected CharacterObjectInteraction(MonoBehaviour monoBehaviourRef, ICharacter characterEntity, IInteractable targetInteractableObject)
    {
        this.monoBehaviourRef = monoBehaviourRef;
        this.targetInteractableObject = targetInteractableObject as IInteractableObject;
        this.characterEntity = characterEntity;
        characterMovement = characterEntity as IMobile;
        playerEntity = characterEntity as IPlayer;
    }

    public virtual void Initialize()
    {
        CharacterEnvironmentInteractionService.Event += OnInteractionEvent;

        if (!(targetInteractableObject is null))
            targetInteractableObjectPosition = GetTargetObjectPosition();
        useObjectSide = GetUseObjectSide();

        if (GetInitialPositioningMethod() is null)
            StartInteraction();
        else
            monoBehaviourRef.StartCoroutine(GetInitialPositioningMethod());
    }

    public void SetHandPositionIK()
    {
        if (targetInteractableObject is null || characterMovement.IsRunning) return;
        characterEntity.Animator.SetIKPositionWeight(AvatarIKGoal.RightHand, characterEntity.Animator.GetFloat(HandWeight));
        characterEntity.Animator.SetIKPosition(AvatarIKGoal.RightHand, targetInteractableObjectPosition);
    }

    public void SetHandWeight()
    {
        characterEntity.Animator.SetFloat(HandWeight, targetIKWeight, 0.2f, Time.deltaTime);
    }

    public virtual void StartInteraction()
    {
        SetMirrorAnimation(IsCharacterOnTheOppositeSide());
    }

    public virtual void FinishInteraction()
    {
        CharacterEnvironmentInteractionService.Event -= OnInteractionEvent;
        CameraUtils.DisableCameraRecenter();

        characterMovement.CanRotate = true;
        characterMovement.CanMove = true;
        characterMovement.CanJump = true;

        if (targetInteractableObject is null) return;
        targetInteractableObject.Collider.enabled = true;
        targetInteractableObject.TriggerAreaCollider.enabled = true;
        targetInteractableObject.CanShowVisualMarker = true;

        playerEntity.IsInteracting = false;
        playerEntity.Rigidbody.interpolation = RigidbodyInterpolation.None;
        playerEntity.Rigidbody.isKinematic = false;
    }

    protected abstract void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactableObject);

    protected abstract IEnumerator GetInitialPositioningMethod();

    protected IEnumerator SetCharacterLookRotation()
    {
        interpolatingTimer = 0;
        playerEntity.Rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        Vector3 characterOrientation = Quaternion
            .LookRotation(
                targetInteractableObjectPosition - characterEntity.Transform.position, Vector3.up).eulerAngles;
        characterOrientation.x = 0;
        characterOrientation.z = 0;
        Quaternion startRotation = playerEntity.Rigidbody.rotation;

        StartInteraction();

        while (interpolatingTimer < ROTATING_DURATION)
        {
            playerEntity.Rigidbody.rotation =
                Quaternion.Lerp(startRotation, Quaternion.Euler(characterOrientation), interpolatingTimer / ROTATING_DURATION);
            interpolatingTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        playerEntity.Rigidbody.rotation = Quaternion.Euler(characterOrientation);
        playerEntity.Rigidbody.interpolation = RigidbodyInterpolation.None;
    }

    protected IEnumerator SetCharacterPosition()
    {
        interpolatingTimer = 0;

        playerEntity.Rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
        Vector2 characterPositionOffset = GetCharacterPositionOffset();
        Quaternion targetRotation = targetInteractableObject.Transform.rotation;

        if (IsCharacterOnTheOppositeSide()) //TODO test this and make it a function
        {
            characterPositionOffset.y *= -1;
            Vector3 rotationEulerAngles = targetRotation.eulerAngles;
            rotationEulerAngles.x *= -1;
            rotationEulerAngles.y -= 180;
            rotationEulerAngles.z *= -1;
            targetRotation = Quaternion.Euler(rotationEulerAngles);
        }


        Vector3 targetPosition = targetInteractableObjectPosition +
                                 targetInteractableObject.Transform.TransformDirection(characterPositionOffset.x,
                                     0, //TODO investigate this. maybe transformPoint works better
                                     characterPositionOffset.y);


        Vector3 startPosition = characterEntity.Transform.position;
        Quaternion startRotation = playerEntity.Rigidbody.rotation;
        while (interpolatingTimer < POSITIONING_DURATION)
        {
            targetPosition.y = characterEntity.Transform.position.y;
            playerEntity.Rigidbody.MovePosition(
                Vector3.Lerp(startPosition, targetPosition, interpolatingTimer / POSITIONING_DURATION));
            playerEntity.Rigidbody.MoveRotation(
                Quaternion.Lerp(startRotation, targetRotation, interpolatingTimer / POSITIONING_DURATION));

            interpolatingTimer += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        playerEntity.Rigidbody.MovePosition(targetPosition);
        playerEntity.Rigidbody.MoveRotation(targetRotation);
        playerEntity.Rigidbody.interpolation = RigidbodyInterpolation.None;

        if (GetEnableCameraRecenter())
            CameraUtils.EnableCameraRecenter(monoBehaviourRef, 0.5f, 0.5f, 2.0f);
        StartInteraction();
    }

    protected virtual bool IsCharacterOnTheOppositeSide()
    {
        if (!useObjectSide) return false;
        return !(CharacterEnvironmentInteractionService.GetCharacterOffsetFromInteractable(targetInteractableObject, characterEntity).z < 0);
    }

    protected virtual Vector2 GetCharacterPositionOffset()
    {
        return targetInteractableObject.CharacterPositionOffset;
    }

    protected virtual bool GetUseObjectSide()
    {
        return false;
    }

    protected virtual Vector3 GetTargetObjectPosition()
    {
        return targetInteractableObject.ObjectCenter;
    }

    protected virtual bool GetEnableCameraRecenter()
    {
        return true;
    }

    protected void SetCharacterKinematic()
    {
        playerEntity.Rigidbody.isKinematic = true;
    }

    private void SetMirrorAnimation(bool state)
    {
        if (targetInteractableObject.Animator) targetInteractableObject.Animator.SetBool(Mirror, state);
        characterEntity.Animator.SetBool(Mirror, state);
    }
}