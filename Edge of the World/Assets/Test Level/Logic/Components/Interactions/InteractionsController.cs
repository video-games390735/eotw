﻿using System;
using System.Collections;
using UnityEngine;

public class InteractionsController : MonoBehaviour
{
    private IInteractable closestInteractable;

    private ICharacterEnvironmentInteraction interactionInstance;

    private IEnumerator interactionRoutine;

    private void Start()
    {
        CharacterEnvironmentInteractionService.Event += OnInteractionEvent;
        AnimationEventsService.Event += OnAnimationEvent;
    }

    private void OnDestroy()
    {
        CharacterEnvironmentInteractionService.Event -= OnInteractionEvent;
        AnimationEventsService.Event -= OnAnimationEvent;
    }

    private void OnInteractionEvent(InteractionEventTypes eventType, ICharacter character, IInteractable interactable = null)
    {
        switch (eventType)
        {
            case InteractionEventTypes.CharacterInteraction:
                StartInteraction(character);
                break;
            case InteractionEventTypes.CharacterTeleport:
                if (interactionInstance is null || closestInteractable is null) return;
                interactionInstance.FinishInteraction();
                CharacterEnvironmentInteractionService.NotifyEvent(InteractionEventTypes.CharacterExit, character, closestInteractable);
                break;
        }
    }

    private void StartInteraction(ICharacter character)
    {
        if (CharacterEnvironmentInteractionService.EmitterStack.Count < 1 ||
            ((IPlayer) character).IsInteracting || !((IMobile) character).IsGrounded) return;
        closestInteractable = CharacterEnvironmentInteractionService.GetClosestObjectToPlayer();

        if (closestInteractable is null) return;
        SetCharacterProperties(character);
        SetInteractableObjectProperties();

        interactionInstance = GetInteractionInstance(character);
        interactionInstance.Initialize();
    }

    private void OnAnimationEvent(AnimationEventTypes eventType, ICharacter character)
    {
        if (interactionInstance is null) return;
        switch (eventType)
        {
            case AnimationEventTypes.ParamChanges:
                interactionInstance.SetHandWeight();
                break;
            case AnimationEventTypes.AnimatorIK:
                interactionInstance.SetHandPositionIK();
                break;
        }
    }

    private ICharacterEnvironmentInteraction GetInteractionInstance(ICharacter character)
    {
        switch (closestInteractable)
        {
            case IInteractableObject interactableObject:
                switch (interactableObject.ObjectType)
                {
                    case InteractableObjectTypes.GroundItem:
                    case InteractableObjectTypes.SurfaceItem:
                        return new CharacterItemInteraction(this, character, closestInteractable);
                    case InteractableObjectTypes.Bed:
                    case InteractableObjectTypes.Bench:
                        return new CharacterSurfaceInteraction(this, character, closestInteractable);
                    case InteractableObjectTypes.Door:
                        return new CharacterDoorInteraction(this, character, closestInteractable);
                    case InteractableObjectTypes.Ladder:
                        return new CharacterLadderInteraction(this, character, closestInteractable);
                    case InteractableObjectTypes.MovableObject:
                        return new CharacterMovableObjectInteraction(this, character, closestInteractable);
                    default:
                        return null;
                }
                case ICharacter interactableCharacter:
                    return new PlayerDwellerInteraction(this, character, interactableCharacter);
        }

        return null;
    }

    private void SetInteractableObjectProperties()
    {
        switch (closestInteractable)
        {
            case IInteractableObject interactableObject:
                interactableObject.Collider.enabled = false;
                interactableObject.CanShowVisualMarker = false;
                break;
            case ICharacter interactableObject:
                interactableObject.Collider.enabled = false;
                break;
        }

        closestInteractable.TriggerAreaCollider.enabled = false;
    }

    private static void SetCharacterProperties(ICharacter character)
    {
        IMobile characterMovement = character as IMobile;
        ((IPlayer) character).IsInteracting = true;

        if (characterMovement == null) return;
        characterMovement.IsCrouching = false; //TODO add can crouch
        characterMovement.CanRotate = false;
        characterMovement.CanMove = false;
        characterMovement.CanJump = false;
    }
}